class AddBlockedToDumpedFiles < ActiveRecord::Migration[5.0]
  def change
    add_column :dumped_files, :blocked, :boolean, default: false
  end
end
