class ThawRequestsController < ApplicationController
	before_action :authenticate_admin
	def index
		@thaw_requests = ThawRequest.order("created_at DESC").includes(:referer, :user_agent).page(params[:page]).per(100).without_count
	end
end
