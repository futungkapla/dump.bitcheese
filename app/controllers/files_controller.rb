require_dependency 'dump'

class FilesController < ApplicationController
	def upload
		uploaded = params[:file]
		if !Dump.get_upload_permission && !simple_captcha_valid?
			flash[:error] = "Please input correct captcha"
			redirect_to root_url
		elsif !uploaded.is_a? ActionDispatch::Http::UploadedFile
			flash[:error] = "This is not a file"
			redirect_to root_url
		elsif uploaded.size > Settings.max_file_size
			flash[:error] = "File is too big!"
			redirect_to root_url
		else
			hash_of_file = Digest::SHA512.file(uploaded.path).digest
			file = nil
			if (file = DumpedFile.find_by(file_hash: hash_of_file, size: uploaded.size)) && (!file.file_frozen || file.blocked)
				if file.blocked
					flash[:error] = "This file is deleted and blocked. Please do not upload it again."
					redirect_to root_url
				elsif request.query_string == "simple"
					render plain: url_for(controller: "application", action: "index") + file.filename
				else
					redirect_to url_for(controller: "application", action: "index") + file.filename + "/preview"
				end
			else
				cleaned_name = Dump.clean_name(uploaded.original_filename)
				file_key = Dump.gen_suitable_key(Settings.key_size, lambda do |f| !File.exists?(File.join(Settings.dir, "files", f, cleaned_name)) end)
				file_name = File.join(Settings.dir, "files", file_key, cleaned_name)
				
				if !file
					FileUtils.mkdir_p File.join(Settings.dir, "files", file_key)
					File.open(file_name, "wb") do |f| f.write uploaded.read end
				else
					FileUtils.mkdir_p File.join(file.file_path.split("/")[0..-2].join("/"))
					File.open(file.file_path, "wb") do |f| f.write uploaded.read end
				end
				# Success! Log it.
				Upload.transaction do
					u = Upload.new
					u.ip = request.remote_ip
					u.filename = if file
						file.filename
					else
						File.join("files", file_key, cleaned_name)
					end
					u.user_agent = UserAgent.obtain(request.user_agent)
					u.size = uploaded.size
					u.save!
					file ||= DumpedFile.find_or_initialize_by(filename: u.filename)
					file.size = uploaded.size
					file.accessed_at = DateTime.now
					file.file_hash = Digest::SHA512.file(file.file_path).digest
					file.file_frozen = false
					file.save!
				end
				if request.query_string == "simple"
					render plain: url_for(controller: "application", action: "index") + file.filename
				else
					redirect_to url_for(controller: "application", action: "index") + file.filename + "/preview"
				end
			end
		end
	end
	
	def preview
		@slug = Dump.clean_name(params[:slug].to_s)
		@filename = Dump.clean_name(params[:filename])
		@dumped_file = DumpedFile.find_by(filename: "files/#{@slug ? @slug + "/" : ""}#{@filename}")
		return not_found unless @dumped_file
	end
	
	def download
		fname = File.join("files", Dump.clean_name(params[:slug].to_s), Dump.clean_name(params[:filename]))
		filename = File.join(Settings.dir, "files",Dump.clean_name(params[:slug].to_s),Dump.clean_name(params[:filename]))

		Download.transaction do
			f = DumpedFile.find_or_initialize_by(filename: fname)
# 			
			if !File.file?(filename) && f.blocked
				return blocked
			elsif !File.file?(filename) && f.file_frozen
				return unless stale? last_modified: Time.at(0)
				@thaw_request = f.thaw!(request)
				return(render('files/thawin', status: 503, formats: ["html"]))
			elsif f.file_frozen
				f.mark_thawed!
				ThawRequest.where(filename: fname, finished: false).update_all(finished: true)
			elsif !File.file?(filename) 
				return not_found
			end
			if !request.referer.to_s.start_with?(root_url(only_path: false))
				u = Download.new
				u.ip = request.remote_ip
				u.filename = fname
				u.user_agent = UserAgent.obtain(request.user_agent)
				u.referer = Referer.obtain(request.referer)
				u.size = File.size(filename)
				u.save!
			end
			
			if f.new_record?
				f.file_hash = Digest::SHA512.file(f.file_path).digest
			end
			f.size = File.size(filename)
			f.accessed_at = DateTime.now
			f.save!
		end
		send_file filename, x_sendfile: true, disposition: "inline", type: Dump.get_content_type(filename)
	end
end
