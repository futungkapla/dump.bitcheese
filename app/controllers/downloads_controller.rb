class DownloadsController < ApplicationController
	before_action :authenticate_admin
	def index
		@downloads = Download.order("created_at DESC").includes(:user_agent, :referer).page(params[:page]).per(100).without_count
	end
end
